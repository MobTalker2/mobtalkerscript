/*
 * SPDX-FileCopyrightText: 2013-2020 Chimaine, MobTalkerScript contributors
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */
package net.mobtalker.mobtalkerscript.v3;

public abstract class Reference
{
    public static abstract class FunctionNames
    {
        public static final String COMMAND_SAY = "ShowText";
        public static final String COMMAND_MENU = "ShowMenu";
        public static final String COMMAND_SHOW = "ShowSprite";
        public static final String COMMAND_SCENE = "ShowScene";
        public static final String COMMAND_HIDE = "HideTexture";
    }
}
