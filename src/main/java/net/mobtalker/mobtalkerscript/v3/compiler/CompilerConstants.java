/*
 * SPDX-FileCopyrightText: 2013-2020 Chimaine, MobTalkerScript contributors
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */
package net.mobtalker.mobtalkerscript.v3.compiler;

public class CompilerConstants
{
    public static final String G = "_G";
    public static final String ENV = "_ENV";
    
    // ========================================
    
    private CompilerConstants()
    {}
}
