/*
 * SPDX-FileCopyrightText: 2013-2020 Chimaine, MobTalkerScript contributors
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */
/**
 * Package planned for compiling MTS scripts into native Java bytecode.
 */
package net.mobtalker.mobtalkerscript.v3.bytecode;