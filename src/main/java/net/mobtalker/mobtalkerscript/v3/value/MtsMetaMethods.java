/*
 * SPDX-FileCopyrightText: 2013-2020 Chimaine, MobTalkerScript contributors
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */
package net.mobtalker.mobtalkerscript.v3.value;

public final class MtsMetaMethods
{
    public static final MtsString __metatable = MtsString.of( "__metatable" );
    public static final MtsString __index = MtsString.of( "__index" );
    public static final MtsString __newindex = MtsString.of( "__newindex" );
    public static final MtsString __call = MtsString.of( "__call" );
    public static final MtsString __len = MtsString.of( "__len" );
    public static final MtsString __unm = MtsString.of( "__unm" );
    public static final MtsString __add = MtsString.of( "__add" );
    public static final MtsString __sub = MtsString.of( "__sub" );
    public static final MtsString __mul = MtsString.of( "__mul" );
    public static final MtsString __div = MtsString.of( "__div" );
    public static final MtsString __mod = MtsString.of( "__mod" );
    public static final MtsString __pow = MtsString.of( "__pow" );
    public static final MtsString __concat = MtsString.of( "__concat" );
    public static final MtsString __eq = MtsString.of( "__eq" );
    public static final MtsString __lt = MtsString.of( "__lt" );
    public static final MtsString __lte = MtsString.of( "__lte" );
    public static final MtsString __tostring = MtsString.of( "__tostring" );
    
    // ========================================
    
    private MtsMetaMethods()
    {}
}